# player

Music sheet web component. The npm `tanana` package.

## Usage
```html
<meta charset="utf-8">
<title>tanana-player demo</title>
<!-- you'll need the vue script 334Kb -->
<script src="https://unpkg.com/vue"></script>
<!-- plus our script 1.4MB -->
<script src="node_modules/tanana/dist/tanana-player.min.js"></script>

<!-- then you can use the web component, like this -->
<tanana-player music-xml="path/to/sheet.xml"></tanana-player>
```
The required `music-xml` attribute is a [music xml](https://www.musicxml.com/) file. We plan on releasing a npm package converter from other popular music score software file formats.

## Project setup
Please use [nvm](https://github.com/nvm-sh/nvm#installing-and-updating) to avoid node version conflicts between your local projects
```
nvm use
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Tests web component being used
```
npm run wc-test
```

### Lints and fixes files
```
npm run lint
```

### Contributing

Please develop and run tests in the `test` folder.

```
npm test
```
