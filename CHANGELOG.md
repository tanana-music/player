# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.4.0] - 2020-05-12

### Added
- This CHANGELOG file.
- `.nvmrc` file
- Function tananaPlayerStore.checkCursorScroll that scrolls the cursor into view if necessary

### Changed
- Fixed erratic scroll bug, but the scroll velocity still is a bit too high

### Removed
- Function tananaPlayerStore.resetAnimationAction()
- Function tananaPlayerStore.stopAnimationScrollAction()
- Function tananaPlayerStore.setDistanceToScrollAction()
- Function tananaPlayerStore.incrementScrollFrameIdAction()
- Function tananaPlayerStore.setScrollAnimationIdAction()

## [0.3.4] - 2020-04-16

### Changed
- Changed criteria for scrolling when cursor in the end (or beginning) of the page

[Unreleased]: https://gitlab.com/tanana-music/player/-/compare/v0.3.4...master
[v0.3.4]: https://gitlab.com/tanana-music/player/-/compare/v0.3.2...v0.3.4
[v0.3.2]: https://gitlab.com/tanana-music/player/-/compare/v0.3.3...v0.3.2
[v0.3.3]: https://gitlab.com/tanana-music/player/-/compare/v0.3.1...v0.3.3
[v0.3.1]: https://gitlab.com/tanana-music/player/-/compare/v0.3.0...v0.3.1
[v0.3.0]: https://gitlab.com/tanana-music/player/-/compare/v0.2.4...v0.3.0
[v0.2.4]: https://gitlab.com/tanana-music/player/-/compare/v0.2.3...v0.2.4
[v0.2.3]: https://gitlab.com/tanana-music/player/-/compare/v0.2.2...v0.2.3
[v0.2.2]: https://gitlab.com/tanana-music/player/-/compare/v0.2.1...v0.2.2
[v0.2.1]: https://gitlab.com/tanana-music/player/-/compare/v0.2.0...v0.2.1
[v0.2.0]: https://gitlab.com/tanana-music/player/releases/tag/v0.2.0

