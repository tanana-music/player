const mockMoveCursorOnClick = jest.fn()
const mockStepRecursive = jest.fn()

jest.mock('opensheetmusicdisplay')

jest.mock('@/services/player/move-cursor-on-click', () => ({
  moveCursorOnClick: jest.fn((a) => mockMoveCursorOnClick(a))
}))

jest.mock('@/services/player/step-recursive', () => ({
  stepRecursive: jest.fn((a) => mockStepRecursive(a))
}))

import store from '@/services/store/index'
import { OpenSheetMusicDisplay } from 'opensheetmusicdisplay'

describe('store', () => {
  it('is an object', () => {
    expect(typeof store).toBe('object')
  })
  it('has a state object', () => {
    expect(store).toHaveProperty('state')
    expect(typeof store.state).toBe('object')
  })
  it('has a osmd object', () => {
    expect(store).toHaveProperty('osmd')
    expect(typeof store.osmd).toBe('object')
  })
})

describe('store.initOsmdAction()', () => {
  it('changes the osmd value in the store', () => {
    const newOsmd = new OpenSheetMusicDisplay()
    store.initOsmdAction(newOsmd)
    expect(store.osmd).toMatchObject(newOsmd)
  })
  it('chages the necessary state flags', () => {
    store.initOsmdAction(new OpenSheetMusicDisplay())
    expect(store.state.started).toBeFalsy()
    expect(store.state.playing).toBeFalsy()
  })
  it('binds the move-cursor-on-click event to the score', () => {
    store.initOsmdAction(new OpenSheetMusicDisplay())
    expect(mockMoveCursorOnClick).toHaveBeenCalledWith(store)
  })
})

describe('store.startAction', () => {
  const mockShowCursor = jest.fn()
  beforeEach(() => {
    const mockOsmd = new OpenSheetMusicDisplay()
    mockOsmd.cursor = {
      ...mockOsmd.cursor,
      show: mockShowCursor
    }
    store.initOsmdAction(mockOsmd)
  })

  it('changes the necessary state flags', () => {
    store.startAction()
    expect(store.state.started).toBeTruthy()
    expect(store.state.done).toBeFalsy()
  })
  it('shows the osmd cursor', () => {
    store.startAction()
    expect(mockShowCursor).toBeCalled()
  })
  it('triggers the loop step recursive for the player', () => {
    store.startAction()
    expect(mockStepRecursive).toBeCalledWith(store)
  })
})
describe('store.playAction', () => {
  const mockShowCursor = jest.fn()
  beforeEach(() => {
    const mockOsmd = new OpenSheetMusicDisplay()
    mockOsmd.cursor = {
      ...mockOsmd.cursor,
      show: mockShowCursor
    }
    store.initOsmdAction(mockOsmd)
  })

  it('changes the necessary state flags', () => {
    store.startAction()
    expect(store.state.playing).toBeTruthy()
  })
  it('shows the osmd cursor if it hasn\'t being started yet', () => {
    store.state.started = false
    store.startAction()
    expect(mockShowCursor).toBeCalled()
  })
  it('triggers the loop step recursive for the player', () => {
    store.startAction()
    expect(mockStepRecursive).toBeCalledWith(store)
  })
})

describe('store.pauseAction', () => {
  it('changes the necessary state flags', () => {
    store.initOsmdAction(new OpenSheetMusicDisplay())
    store.startAction()
    store.pauseAction()
    expect(store.state.playing).toBeFalsy()
  })
})

describe('store.endAction', () => {
  it('changes the necessary state flags', () => {
    store.initOsmdAction(new OpenSheetMusicDisplay())
    store.startAction()
    store.endAction()
    expect(store.state.done).toBeTruthy()
    expect(store.state.playing).toBeFalsy()
    expect(store.state.started).toBeFalsy()
  })
  it('resets the cursor', () => {
    const mockReset = jest.fn()
    const mockOsmd = new OpenSheetMusicDisplay()
    mockOsmd.cursor = {
      ...mockOsmd.cursor,
      reset: mockReset,
    }
    store.initOsmdAction(mockOsmd)
    store.startAction()
    store.endAction()
    expect(mockReset).toBeCalled()
  })
  it('hides the cursor', () => {
    const mockHide = jest.fn()
    const mockOsmd = new OpenSheetMusicDisplay()
    mockOsmd.cursor = {
      ...mockOsmd.cursor,
      hide: mockHide
    }
    store.initOsmdAction(mockOsmd)
    store.startAction()
    store.endAction()
    expect(mockHide).toBeCalled()
  })
})

describe('store.cursorNextPositionAction', () => {
  it('calls the next() function in the osmd cursor', () => {
    const mockNext = jest.fn()
    const mockOsmd = new OpenSheetMusicDisplay()
    mockOsmd.cursor = {
      ...mockOsmd.cursor,
      next: mockNext
    }
    store.initOsmdAction(mockOsmd)
    store.cursorNextPositionAction()
    expect(mockNext).toBeCalled()
  })
})

describe('store.getCursorCurrentPosition', () => {
  it('returns the DOM current position of the osmd cursor', () => {
    const mockPos = { x: 1, y: 1}
    const mockOsmd = new OpenSheetMusicDisplay()
    mockOsmd.cursor = {
      cursorElement: {
        getBoundingClientRect() {
          return mockPos
        }
      }
    }
    store.initOsmdAction(mockOsmd)
    expect(store.getCursorCurrentPosition()).toEqual(mockPos)
  })
})

describe('store.checkCursorScroll', () => {
  const mockPos = { x: 1, y: 540 }
  const mockOsmd = new OpenSheetMusicDisplay()
  const mockScrollIntoView = jest.fn()
  mockOsmd.container = {
    clientHeight: 600,
    querySelector () {
      return {
        querySelectorAll () { return [] },
        getBoundingClientRect() { return mockPos }
      }
    }
  }
  mockOsmd.cursor = {
    cursorElement: {
      scrollIntoView: mockScrollIntoView,
      getBoundingClientRect() { return mockPos }
    }
  }
  beforeEach(() => {
    mockScrollIntoView.mockReset()
  })
  it('scrolls the cursor if it is at the end of the page', () => {
    store.initOsmdAction(mockOsmd)
    store.checkCursorScroll()
    expect(mockScrollIntoView).toBeCalled()
  })
  it('scrolls the cursor if it is at the begining of the page', () => {
    mockPos.y = 30
    store.initOsmdAction(mockOsmd)
    store.checkCursorScroll()
    expect(mockScrollIntoView).toBeCalled()
  })
  it('do nothing if the cursor is at the middle of the page', () => {
    mockPos.y = 400
    store.initOsmdAction(mockOsmd)
    store.checkCursorScroll()
    expect(mockScrollIntoView).not.toBeCalled()
  })
})
