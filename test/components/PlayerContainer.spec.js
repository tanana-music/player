import { mount } from '@vue/test-utils'
import store from '@/services/store/index'
import PlayerContainer from '@/components/PlayerContainer.vue'

jest.mock('opensheetmusicdisplay')

describe('PlayerContainer', () => {
  it('is a Vue instance', () => {
    const wrapper = mount(PlayerContainer)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
  it('renders correctly', () => {
    const wrapper = mount(PlayerContainer)
    expect(wrapper.element).toMatchSnapshot()
  })
})
