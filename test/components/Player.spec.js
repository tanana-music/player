import { mount } from '@vue/test-utils'
import store from '@/services/store/index'

jest.mock('opensheetmusicdisplay')

import { OpenSheetMusicDisplay } from 'opensheetmusicdisplay'
const mockLoad = jest.fn().mockResolvedValue({})
const mockRender = jest.fn().mockResolvedValue({})

import Player from '@/components/Player.vue'

describe('Player', () => {
  beforeAll(() => {
    jest.spyOn(OpenSheetMusicDisplay.prototype, 'load')
      .mockImplementation(mockLoad)
    jest.spyOn(OpenSheetMusicDisplay.prototype, 'render')
      .mockImplementation(mockRender)
  })
  afterAll(() => { // you're my wonderwall
    jest.restoreAllMocks()
  })
  it('is a Vue instance', () => {
    const wrapper = mount(Player)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('loads the given score', async () => {
    const musicXml = '<xml-test>'
    const wrapper = mount(Player, {
      propsData: { musicXml },
    })
    await wrapper.vm.$nextTick()
    expect(mockLoad).toHaveBeenCalledWith(musicXml, true)
  })

  it('renders the score', async () => {
    const wrapper = mount(Player)
    //TODO: remove workaround to wait two promises
    await wrapper.vm.$nextTick()
    await wrapper.vm.$nextTick()
    expect(mockRender).toBeCalled()
  })

  it('renders a new score when changing the xml prop', async () => {
    const musicXml = 'some other xml'
    const wrapper = mount(Player)
    //TODO: remove workaround to wait two promises
    await wrapper.vm.$nextTick()
    await wrapper.vm.$nextTick()
    wrapper.setProps({
      musicXml
    })
    await wrapper.vm.$nextTick()
    await wrapper.vm.$nextTick()
    expect(mockLoad).toHaveBeenCalledWith(musicXml, true)
  })

  it('saves the osmd object in the store', async () => {
    const wrapper = mount(Player)
    //TODO: remove workaround to wait THREE promises
    await wrapper.vm.$nextTick()
    await wrapper.vm.$nextTick()
    await wrapper.vm.$nextTick()
    expect(store.osmd).not.toBeNull()
  })
})
