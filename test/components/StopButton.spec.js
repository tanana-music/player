import { mount } from '@vue/test-utils'
import store from '@/services/store/index'
import StopButton from '@/components/StopButton.vue'

describe('StopButton', () => {
  it('is a Vue instance', () => {
    const wrapper = mount(StopButton)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
  it('calls the end action on click', () => {
    const wrapper = mount(StopButton)
    const mockEndAction = jest.spyOn(store, 'endAction')
      .mockImplementation(() => {})
    wrapper.trigger('click')
    expect(mockEndAction).toBeCalled()
  })
})
