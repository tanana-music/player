import { mount } from '@vue/test-utils'
import store from '@/services/store/index'
import PlayPauseButton from '@/components/PlayPauseButton.vue'

describe('PlayPauseButton', () => {
  it('is a Vue instance', () => {
    const wrapper = mount(PlayPauseButton)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
  it('shows the play button when paused', () => {
    store.state.playing = false
    const wrapper = mount(PlayPauseButton)
    expect(wrapper.find('img').attributes('alt')).toEqual('Play')
  })
  it('shows the pause button when playing', () => {
    store.state.playing = true
    const wrapper = mount(PlayPauseButton)
    expect(wrapper.find('img').attributes('alt')).toEqual('Pause')
  })
  it('calls the play action on click when paused', () => {
    store.state.playing = false
    const wrapper = mount(PlayPauseButton)
    const mockPlayAction = jest.spyOn(store, 'playAction')
      .mockImplementation(() => {})
    wrapper.trigger('click')
    expect(mockPlayAction).toBeCalled()
  })
  it('calls the pause action on click when playing', () => {
    store.state.playing = true
    const wrapper = mount(PlayPauseButton)
    const mockPauseAction = jest.spyOn(store, 'pauseAction')
      .mockImplementation(() => {})
    wrapper.trigger('click')
    expect(mockPauseAction).toBeCalled()
  })
})
