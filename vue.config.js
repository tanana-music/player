module.exports = {
  chainWebpack: config => {
    // xml as string loader
    config.module
      .rule('xml')
      .test(/\.xml$/)
      .use('raw-loader')
        .loader('raw-loader')
        .end()
  }
}
