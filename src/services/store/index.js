import {
  moveCursorOnClick,
  DELTA_X
} from '@/services/player/move-cursor-on-click'
import { stepRecursive } from '@/services/player/step-recursive'

const CURSOR_HEIGHT = 80

global.tananaPlayerStore = {
  osmd: null,
  endOfLine: 0,
  state: {
    waiting: false,
    started: false,
    done: false,
    playing: false
  },
  initOsmdAction (value) {
    delete this.osmd
    this.osmd = value
    this.state.started = false
    this.setWaitingAction(false)
    this.calcEndOfLineAction()
    moveCursorOnClick(this)
  },

  // workaround to ovewrite cursor style
  ovewriteCursorStyleAction () {
    if (this.osmd.cursor.cursorElement.src === '') return
    this.osmd.cursor.cursorElement.src = ''
    this.osmd.cursor.cursorElement.height = CURSOR_HEIGHT
    this.osmd.cursor.cursorElement.width = 10
  },

  setCursorTransitionAction (withTransition) {
    if (withTransition && !this.isCursorAtEndOfLine()) {
      this.osmd.cursor.cursorElement.style.transition = 'left 200ms linear'
    } else {
      this.osmd.cursor.cursorElement.style.transition = 'none'
    }
  },

  calcEndOfLineAction () {
    const scoreSvg = this.osmd.container.querySelector('svg')
    const linesOfSvg = Array.from(scoreSvg.querySelectorAll('rect'))
    const positionsOfLines = linesOfSvg.map(({ x }) => x.baseVal.value)
    const { x } = scoreSvg.getBoundingClientRect()
    this.endOfLine = x + Math.max(...positionsOfLines)
  },

  startAction () {
    this.osmd.cursor.show()
    this.ovewriteCursorStyleAction()
    this.state.started = true
    this.state.done = false
    this.playAction()
  },

  playAction () {
    if (!this.state.started) return this.startAction()
    if (this.state.done) return
    this.state.playing = true
    stepRecursive(this)
  },

  setWaitingAction (value) {
    this.state.waiting = value
  },

  pauseAction () {
    this.setCursorTransitionAction(false)
    if (this.done) return
    this.state.playing = false
  },

  endAction () {
    this.state.done = true
    this.state.playing = false
    this.state.started = false
    this.setCursorTransitionAction(false)
    this.osmd.cursor.reset()
    this.osmd.cursor.hide()
  },

  cursorNextPositionAction () {
    this.osmd.cursor.next()
    this.ovewriteCursorStyleAction()
  },

  getCursorCurrentPosition () {
    return this.osmd.cursor.cursorElement.getBoundingClientRect()
  },

  isCursorAtEndOfLine () {
    return this.getCursorCurrentPosition().x > this.endOfLine - (2 * DELTA_X)
  },

  checkCursorScroll () {
    const { y } = this.getCursorCurrentPosition()
    const containerHeight = this.osmd.container.clientHeight
    const isBelowTop = y >= CURSOR_HEIGHT
    const isAboveBottom = y < containerHeight - CURSOR_HEIGHT
    if (isBelowTop && isAboveBottom) return
    this.osmd.cursor.cursorElement.scrollIntoView({
      block: 'center',
      behavior: 'smooth'
    })
  }
}

export default global.tananaPlayerStore
