import {
  msToSleep
} from './osmd-utils'

const sleep = async ms => new Promise(resolve => setTimeout(resolve, ms))

async function stepRecursive (store) {
  if (store.state.playing && !store.state.waiting) {
    store.setWaitingAction(true)
    await sleep(msToSleep(store.osmd))
    if (!store.state.playing) {
      store.setWaitingAction(false)
      return
    }
    store.setCursorTransitionAction(true)
    store.cursorNextPositionAction()
    store.checkCursorScroll()
    store.setWaitingAction(false)
    if (store.osmd.cursor.iterator.endReached) store.endAction()
    else await stepRecursive(store)
  }
}

export {
  stepRecursive
}
