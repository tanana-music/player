const DELTA_X = 30
const DELTA_Y = 150

const restartAndPause = (store) => {
  store.endAction()
  store.playAction()
  store.pauseAction()
}

const cursorIsCloseEnough = (evPos, cursorPos) =>
  (Math.abs(evPos.x - cursorPos.x) <= DELTA_X) &&
  (Math.abs(evPos.y - cursorPos.y) <= DELTA_Y)

const moveCursor = store => (ev) => {
  console.log('click', ev)
  if (store.state.playing) {
    return
  }
  restartAndPause(store)
  let bounding = store.getCursorCurrentPosition()
  while (!cursorIsCloseEnough(ev, bounding)) {
    if (store.osmd.cursor.iterator.endReached) {
      // prevent infinite loop and force start of score
      restartAndPause(store)
      break
    }
    store.cursorNextPositionAction()
    bounding = store.getCursorCurrentPosition()
  }
}

const moveCursorOnClick = (store) => store.osmd.container
  .addEventListener('click', moveCursor(store))

export {
  moveCursorOnClick,
  DELTA_X
}
