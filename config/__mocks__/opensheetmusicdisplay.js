const getBoundingClientRect = () => ({ x: 0, y: 0 })
export class OpenSheetMusicDisplay {
  constructor(element) {
    this.cursor = {
      show() {},
      reset() {},
      hide() {},
      next() {},
      cursorElement: {
        getBoundingClientRect,
        src: '',
        style: {
          transition: ''
        }
      }
    }
    this.container = {
      addEventListener() {},
      querySelector() {
        // mock svg content
        return {
          getBoundingClientRect,
          querySelectorAll() {
            return [
              { x: { baseVal: { value: 0 } } }
            ]
          }
        }
      }
    }
  }
  async load(musicXml, flag) {
  }
  async render() {}
}
